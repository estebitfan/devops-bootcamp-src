'use strict';

var express = require('express');
var app = express();
var os = require("os")

// The code below will display
// Hello World!' to the browser when you go to http://localhost:3000
app.get('/', function(req, res){
  res.send('<h1> Hola Bootcamp </h1>');
});

app.get('/health', function(req, res){
  res.send({'status':'ok'});
});

app.get('/version', function(req, res){
    res.send({
        'version': `${process.env.COMMIT || "000"}`, 
        'hostname':`${os.hostname}`
    });
  });

app.listen(3000, function(){
  console.log('Example app listening on port 3000!');
});

module.exports = app;
