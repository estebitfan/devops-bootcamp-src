var request = require('supertest');


var app = require('../src/server.js');

describe('GET /', function() {
    it('displays "Hola Bootcamp!"', function (done) {
        // The line below is the core test of our app.
        request(app).get('/')
     .expect('<h1> Hola Bootcamp </h1>')
     .expect(200, done);
    });
});



describe('GET /health', function() {
    it('status is  "ok"', function (done) {
        request(app).get('/health')
      .expect({ 'status': 'ok' })
      .expect(200, done);
    });
});
